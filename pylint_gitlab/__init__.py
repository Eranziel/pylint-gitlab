#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

"""Module ``pylint_gitlab``."""

from pylint_gitlab.reporter import GitlabCodeClimateReporter, GitlabPagesHtmlReporter
